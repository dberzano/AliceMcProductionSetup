void rec(Bool_t useHLT= kTRUE) {
  AliReconstruction reco;

  reco.SetRunReconstruction("ITS TPC TRD TOF PHOS HMPID EMCAL MUON ZDC PMD T0 VZERO HLT");

//
// switch off cleanESD, write ESDfriends and Alignment data
//
  reco.SetCleanESD(kFALSE);
  reco.SetWriteESDfriend();
  reco.SetWriteAlignmentData();
  reco.SetFractionFriends(.1);

  reco.SetWriteESDfriend(kFALSE);
  reco.SetWriteAlignmentData();

//
// RAW OCDB
//
  reco.SetDefaultStorage("alien://Folder=/alice/data/2011/OCDB");
  // reco.SetCDBSnapshotMode("OCDB_MCrec.root");

//
// ITS (2 objects)
//
  reco.SetSpecificStorage("ITS/Align/Data",          "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("ITS/Calib/SPDSparseDead", "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");

//
// MUON (1 object)
//
  reco.SetSpecificStorage("MUON/Align/Data",         "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");

//
// TPC (7 objects)
//
  reco.SetSpecificStorage("TPC/Align/Data",          "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/ClusterParam",  "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/RecoParam",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/TimeGain",      "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/AltroConfig",   "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/TimeDrift",     "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");
  reco.SetSpecificStorage("TPC/Calib/Correction",    "alien://Folder=/alice/simulation/2008/v4-15-Release/Residual");

  reco.SetRecoParam("ZDC",AliZDCRecoParamPbPb::GetHighFluxParam(2760));

  // Introduce the TPC bug
  reco.SetOption("TPC","IntroduceBug");

  // reco.SetRunQA(":");

  // -------------------------------------------------------
  reco.SetOption("TPC", "useHLT");
  // -------------------------------------------------------

  TStopwatch timer;
  timer.Start();

  reco.Run();

  timer.Stop();
  timer.Print();
}
